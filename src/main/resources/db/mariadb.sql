# Create schemas

# Create tables
CREATE TABLE IF NOT EXISTS User
(
    id BIGINT NOT NULL AUTO_INCREMENT,
    firstName VARCHAR(200),
    lastName VARCHAR(200),
    email VARCHAR(200),
    password VARCHAR(1000),
    enabled TINYINT(1),
    verified TINYINT(1),
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS Role
(
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(200),
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS Permission
(
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(200),
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS RolesPermissions
(
    id BIGINT NOT NULL AUTO_INCREMENT,
    roleId BIGINT,
    permissionId BIGINT,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS UsersRoles
(
    id BIGINT NOT NULL AUTO_INCREMENT,
    userId BIGINT,
    roleId BIGINT,
    PRIMARY KEY(id)
);


# Create FKs
ALTER TABLE RolesPermissions
    ADD    FOREIGN KEY (roleId)
    REFERENCES Role(id)
;
    
ALTER TABLE RolesPermissions
    ADD    FOREIGN KEY (permissionId)
    REFERENCES Permission(id)
;
    
ALTER TABLE UsersRoles
    ADD    FOREIGN KEY (userId)
    REFERENCES User(id)
;
    
ALTER TABLE UsersRoles
    ADD    FOREIGN KEY (roleId)
    REFERENCES Role(id)
;
    

# Create Indexes
