package be.manhart;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import be.manhart.rest.auth.UserController;
import be.manhart.speedment.springbootspeedment.entity.role.Role;

/**
 * here we configure the security settings of our requests. This is a bit
 * redundant since we secured {@link UserController} too.
 * 
 * @author manuel
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// @formatter:off
    	http.authorizeRequests()
                .antMatchers(HttpMethod.POST, "/user").permitAll()
                .antMatchers(HttpMethod.GET, "/user").hasAuthority(Role.ADMIN)
                .antMatchers(HttpMethod.GET, "/user/{id}").hasAuthority(Role.USER)
                .anyRequest().fullyAuthenticated()
            .and().httpBasic()
            .and().csrf().disable();
    	// @formatter:on
	}
}
