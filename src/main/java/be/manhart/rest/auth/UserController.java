package be.manhart.rest.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import be.manhart.rest.BaseRestController;
import be.manhart.rest.exception.ForbiddenException;
import be.manhart.speedment.springbootspeedment.entity.UserWithRoles;
import be.manhart.speedment.springbootspeedment.entity.role.Role;
import be.manhart.speedment.springbootspeedment.entity.role.RoleManager;
import be.manhart.speedment.springbootspeedment.entity.user.User;
import be.manhart.speedment.springbootspeedment.entity.user.UserImpl;
import be.manhart.speedment.springbootspeedment.entity.user.UserManager;
import be.manhart.speedment.springbootspeedment.entity.usersroles.UsersRolesManager;

@RestController()
public class UserController extends BaseRestController {

	private @Autowired UserManager users;
	private @Autowired RoleManager roles;
	private @Autowired UsersRolesManager usersRoles;
	private @Autowired PasswordEncoder passwordEncoder;

	@PostMapping("/user")
	long onPostAccount(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName,
			@RequestParam("email") String email, @RequestParam("password") String password) {
		User user = users.find(email).orElseGet(() -> {
				// create user
				// @formatter:off
				User u = users.create(new UserImpl()
						.setFirstName(firstName)
						.setLastName(lastName)
						.setEmail(email)
						.setVerified(User.FALSE)
						.setEnabled(User.FALSE)
						.setPassword(passwordEncoder.encode(password)));
				// @formatter:on
				// find role
				Role userRole = roles.get(Role.USER);
				// map role to user
				usersRoles.create(u, userRole);
				return u;
			}
		);

		return user.getId();
	}

	@GetMapping("/user")
	List<User> onGetAllUsers(Authentication auth) {
		final UserWithRoles user = (UserWithRoles) auth.getPrincipal();
		if (user.hasRole(Role.ADMIN)) {
			return users.findAll();
		} else {
			throw new ForbiddenException();
		}
	}

	@GetMapping("user/{id}")
	User onGetAccount(@PathVariable("id") int id, Authentication auth) {
		final UserWithRoles user = (UserWithRoles) auth.getPrincipal();
		if (user.getUser().getId() == id) {
			return user.getUser();
		} else if (user.hasRole(Role.ADMIN)) {
			return users.get(id);
		} else {
			throw new ForbiddenException();
		}
	}

}
