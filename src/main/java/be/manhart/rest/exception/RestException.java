package be.manhart.rest.exception;

public abstract class RestException extends RuntimeException {

	public RestException(String message) {
		super(message);
	}

	/**
	 * @return an http status code which will be given back to the client
	 */
	public abstract int getStatusCode();
}
