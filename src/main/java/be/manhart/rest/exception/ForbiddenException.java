package be.manhart.rest.exception;

import org.springframework.http.HttpStatus;

public class ForbiddenException extends RestException {

	public ForbiddenException(String message) {
		super(message);
	}

	public ForbiddenException() {
		super("no.permission");
	}

	@Override
	public int getStatusCode() {
		return HttpStatus.FORBIDDEN.value();
	}
}
