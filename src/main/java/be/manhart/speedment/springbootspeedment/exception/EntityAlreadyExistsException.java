package be.manhart.speedment.springbootspeedment.exception;

/**
 * If an entity should be created but already exists, this exception is thrown.
 * 
 * @author manuel
 */
public class EntityAlreadyExistsException extends DatabaseException {

	public EntityAlreadyExistsException() {
		super("entity.alreadyExists");
	}

	public EntityAlreadyExistsException(String message) {
		super(message);
	}
}
