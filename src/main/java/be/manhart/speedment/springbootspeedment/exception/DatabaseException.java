package be.manhart.speedment.springbootspeedment.exception;

/**
 * All our DB exceptions are subclasses of this one.
 * 
 * @author manuel
 */
public class DatabaseException extends RuntimeException {

	public DatabaseException(String message) {
		super(message);
	}
}
