package be.manhart.speedment.springbootspeedment.exception;

import java.util.Optional;

import com.speedment.runtime.core.manager.Manager;

/**
 * This is thrown when a {@link Manager}s get function is called and no entity
 * could be found. For {@link Optional} requests to the DB a find method should
 * be used.
 * 
 * @author manuel
 */
public class EntityNotFoundException extends DatabaseException {

	public EntityNotFoundException() {
		super("entity.notFound");
	}

	public EntityNotFoundException(String message) {
		super(message);
	}
}
