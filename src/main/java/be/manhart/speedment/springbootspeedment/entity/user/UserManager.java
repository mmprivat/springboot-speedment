package be.manhart.speedment.springbootspeedment.entity.user;

import java.util.List;
import java.util.Optional;

import be.manhart.speedment.springbootspeedment.entity.user.generated.GeneratedUserManager;

/**
 * The main interface for the manager of every
 * {@link be.manhart.speedment.springbootspeedment.entity.user.User} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * <p>
 * I added these functions for encapsulation of speedment code into the managers.
 * 
 * @author be.manhart
 */
public interface UserManager extends GeneratedUserManager {
	User create(User user);

	Optional<User> find(int id);

	Optional<User> find(String email);

	User get(int id);

	User get(String email);

	List<User> findAll();

}