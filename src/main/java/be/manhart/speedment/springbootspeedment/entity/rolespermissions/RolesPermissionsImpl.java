package be.manhart.speedment.springbootspeedment.entity.rolespermissions;

import be.manhart.speedment.springbootspeedment.entity.rolespermissions.generated.GeneratedRolesPermissionsImpl;

/**
 * The default implementation of the {@link
 * be.manhart.speedment.springbootspeedment.entity.rolespermissions.RolesPermissions}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public final class RolesPermissionsImpl 
extends GeneratedRolesPermissionsImpl 
implements RolesPermissions {}