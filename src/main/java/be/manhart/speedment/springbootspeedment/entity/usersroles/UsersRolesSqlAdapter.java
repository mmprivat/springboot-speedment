package be.manhart.speedment.springbootspeedment.entity.usersroles;

import be.manhart.speedment.springbootspeedment.entity.usersroles.generated.GeneratedUsersRolesSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * be.manhart.speedment.springbootspeedment.entity.usersroles.UsersRoles}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public class UsersRolesSqlAdapter extends GeneratedUsersRolesSqlAdapter {}