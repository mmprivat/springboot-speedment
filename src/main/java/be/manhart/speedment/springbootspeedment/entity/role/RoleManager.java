package be.manhart.speedment.springbootspeedment.entity.role;

import java.util.Optional;

import be.manhart.speedment.springbootspeedment.entity.role.generated.GeneratedRoleManager;

/**
 * The main interface for the manager of every {@link
 * be.manhart.speedment.springbootspeedment.entity.role.Role} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * <p>
 * I added these functions for encapsulation of speedment code into the managers.
 * 
 * @author be.manhart
 */
public interface RoleManager extends GeneratedRoleManager {

	Optional<Role> find(String roleName);

	Role get(String roleName);

	Role create(String roleName);

	Role findOrCreate(String admin);
	
}