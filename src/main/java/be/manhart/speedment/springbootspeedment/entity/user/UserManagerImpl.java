package be.manhart.speedment.springbootspeedment.entity.user;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import be.manhart.speedment.springbootspeedment.entity.user.generated.GeneratedUserManagerImpl;
import be.manhart.speedment.springbootspeedment.exception.EntityAlreadyExistsException;
import be.manhart.speedment.springbootspeedment.exception.EntityNotFoundException;

/**
 * The default implementation for the manager of every
 * {@link be.manhart.speedment.springbootspeedment.entity.user.User} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public final class UserManagerImpl extends GeneratedUserManagerImpl implements UserManager {

	@Override
	public User create(User user) {
		String email = user.getEmail().get();
		find(email).ifPresent(u -> {
			throw new EntityAlreadyExistsException("User already present");
		});
		persist(user);
		return get(email);
	}

	@Override
	public Optional<User> find(int id) {
		return stream().filter(User.ID.equal(id)).findAny();
	}

	@Override
	public User get(int id) {
		return find(id).orElseThrow(() -> new EntityNotFoundException("User " + id + " could not be found."));
	}

	@Override
	public Optional<User> find(String email) {
		return stream().filter(User.EMAIL.equal(email)).findAny();
	}

	@Override
	public User get(String email) {
		return find(email).orElseThrow(() -> new EntityNotFoundException("User " + email + " could not be found."));
	}

	@Override
	public List<User> findAll() {
		return stream().collect(Collectors.toList());
	}
}