package be.manhart.speedment.springbootspeedment.entity.rolespermissions.generated;

import be.manhart.speedment.springbootspeedment.entity.role.Role;
import be.manhart.speedment.springbootspeedment.entity.rolespermissions.RolesPermissions;
import com.speedment.common.annotation.GeneratedCode;
import com.speedment.runtime.core.manager.Manager;
import com.speedment.runtime.core.util.OptionalUtil;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.StringJoiner;

/**
 * The generated base implementation of the {@link
 * be.manhart.speedment.springbootspeedment.entity.rolespermissions.RolesPermissions}-interface.
 * <p>
 * This file has been automatically generated by Speedment. Any changes made to
 * it will be overwritten.
 * 
 * @author Speedment
 */
@GeneratedCode("Speedment")
public abstract class GeneratedRolesPermissionsImpl implements RolesPermissions {
    
    private int id;
    private Integer roleId;
    private Integer permissionId;
    
    protected GeneratedRolesPermissionsImpl() {
        
    }
    
    @Override
    public int getId() {
        return id;
    }
    
    @Override
    public OptionalInt getRoleId() {
        return OptionalUtil.ofNullable(roleId);
    }
    
    @Override
    public OptionalInt getPermissionId() {
        return OptionalUtil.ofNullable(permissionId);
    }
    
    @Override
    public RolesPermissions setId(int id) {
        this.id = id;
        return this;
    }
    
    @Override
    public RolesPermissions setRoleId(Integer roleId) {
        this.roleId = roleId;
        return this;
    }
    
    @Override
    public RolesPermissions setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
        return this;
    }
    
    @Override
    public Optional<Role> findRoleId(Manager<Role> foreignManager) {
        if (getRoleId().isPresent()) {
            return foreignManager.stream().filter(Role.ID.equal(getRoleId().getAsInt())).findAny();
        } else {
            return Optional.empty();
        }
    }
    
    @Override
    public String toString() {
        final StringJoiner sj = new StringJoiner(", ", "{ ", " }");
        sj.add("id = "           + Objects.toString(getId()));
        sj.add("roleId = "       + Objects.toString(OptionalUtil.unwrap(getRoleId())));
        sj.add("permissionId = " + Objects.toString(OptionalUtil.unwrap(getPermissionId())));
        return "RolesPermissionsImpl " + sj.toString();
    }
    
    @Override
    public boolean equals(Object that) {
        if (this == that) { return true; }
        if (!(that instanceof RolesPermissions)) { return false; }
        final RolesPermissions thatRolesPermissions = (RolesPermissions)that;
        if (this.getId() != thatRolesPermissions.getId()) {return false; }
        if (!Objects.equals(this.getRoleId(), thatRolesPermissions.getRoleId())) {return false; }
        if (!Objects.equals(this.getPermissionId(), thatRolesPermissions.getPermissionId())) {return false; }
        return true;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Integer.hashCode(getId());
        hash = 31 * hash + Objects.hashCode(getRoleId());
        hash = 31 * hash + Objects.hashCode(getPermissionId());
        return hash;
    }
}