package be.manhart.speedment.springbootspeedment.entity.role;

import be.manhart.speedment.springbootspeedment.entity.role.generated.GeneratedRole;

/**
 * The main interface for entities of the {@code Role}-table in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * <p>
 * The roles are used to check if a user has permission to read his own data
 * ({@link Role#USER}) or even other users data ({@link Role#ADMIN}).
 * 
 * @author be.manhart
 */
public interface Role extends GeneratedRole {
	String USER = "User";
	String ADMIN = "Admin";
}