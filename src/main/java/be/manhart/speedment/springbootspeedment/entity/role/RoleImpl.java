package be.manhart.speedment.springbootspeedment.entity.role;

import be.manhart.speedment.springbootspeedment.entity.role.generated.GeneratedRoleImpl;

/**
 * The default implementation of the {@link
 * be.manhart.speedment.springbootspeedment.entity.role.Role}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public final class RoleImpl 
extends GeneratedRoleImpl 
implements Role {}