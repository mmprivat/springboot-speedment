package be.manhart.speedment.springbootspeedment.entity.usersroles;

import be.manhart.speedment.springbootspeedment.entity.usersroles.generated.GeneratedUsersRoles;

/**
 * The main interface for entities of the {@code UsersRoles}-table in the
 * database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public interface UsersRoles extends GeneratedUsersRoles {}