package be.manhart.speedment.springbootspeedment.entity.rolespermissions.generated;

import be.manhart.speedment.springbootspeedment.entity.role.Role;
import be.manhart.speedment.springbootspeedment.entity.rolespermissions.RolesPermissions;
import com.speedment.common.annotation.GeneratedCode;
import com.speedment.runtime.config.identifier.ColumnIdentifier;
import com.speedment.runtime.config.identifier.TableIdentifier;
import com.speedment.runtime.core.manager.Manager;
import com.speedment.runtime.core.util.OptionalUtil;
import com.speedment.runtime.field.ComparableField;
import com.speedment.runtime.field.ComparableForeignKeyField;
import com.speedment.runtime.field.IntField;
import com.speedment.runtime.typemapper.TypeMapper;
import java.util.Optional;
import java.util.OptionalInt;

/**
 * The generated base for the {@link
 * be.manhart.speedment.springbootspeedment.entity.rolespermissions.RolesPermissions}-interface
 * representing entities of the {@code RolesPermissions}-table in the database.
 * <p>
 * This file has been automatically generated by Speedment. Any changes made to
 * it will be overwritten.
 * 
 * @author Speedment
 */
@GeneratedCode("Speedment")
public interface GeneratedRolesPermissions {
    
    /**
     * This Field corresponds to the {@link RolesPermissions} field that can be
     * obtained using the {@link RolesPermissions#getId()} method.
     */
    IntField<RolesPermissions, Integer> ID = IntField.create(
        Identifier.ID,
        RolesPermissions::getId,
        RolesPermissions::setId,
        TypeMapper.primitive(),
        true
    );
    /**
     * This Field corresponds to the {@link RolesPermissions} field that can be
     * obtained using the {@link RolesPermissions#getRoleId()} method.
     */
    ComparableForeignKeyField<RolesPermissions, Integer, Integer, Role> ROLE_ID = ComparableForeignKeyField.create(
        Identifier.ROLE_ID,
        o -> OptionalUtil.unwrap(o.getRoleId()),
        RolesPermissions::setRoleId,
        Role.ID,
        TypeMapper.identity(),
        false
    );
    /**
     * This Field corresponds to the {@link RolesPermissions} field that can be
     * obtained using the {@link RolesPermissions#getPermissionId()} method.
     */
    ComparableField<RolesPermissions, Integer, Integer> PERMISSION_ID = ComparableField.create(
        Identifier.PERMISSION_ID,
        o -> OptionalUtil.unwrap(o.getPermissionId()),
        RolesPermissions::setPermissionId,
        TypeMapper.identity(),
        false
    );
    
    /**
     * Returns the id of this RolesPermissions. The id field corresponds to the
     * database column
     * springbootspeedment.springbootspeedment.RolesPermissions.id.
     * 
     * @return the id of this RolesPermissions
     */
    int getId();
    
    /**
     * Returns the roleId of this RolesPermissions. The roleId field corresponds
     * to the database column
     * springbootspeedment.springbootspeedment.RolesPermissions.roleId.
     * 
     * @return the roleId of this RolesPermissions
     */
    OptionalInt getRoleId();
    
    /**
     * Returns the permissionId of this RolesPermissions. The permissionId field
     * corresponds to the database column
     * springbootspeedment.springbootspeedment.RolesPermissions.permissionId.
     * 
     * @return the permissionId of this RolesPermissions
     */
    OptionalInt getPermissionId();
    
    /**
     * Sets the id of this RolesPermissions. The id field corresponds to the
     * database column
     * springbootspeedment.springbootspeedment.RolesPermissions.id.
     * 
     * @param id to set of this RolesPermissions
     * @return   this RolesPermissions instance
     */
    RolesPermissions setId(int id);
    
    /**
     * Sets the roleId of this RolesPermissions. The roleId field corresponds to
     * the database column
     * springbootspeedment.springbootspeedment.RolesPermissions.roleId.
     * 
     * @param roleId to set of this RolesPermissions
     * @return       this RolesPermissions instance
     */
    RolesPermissions setRoleId(Integer roleId);
    
    /**
     * Sets the permissionId of this RolesPermissions. The permissionId field
     * corresponds to the database column
     * springbootspeedment.springbootspeedment.RolesPermissions.permissionId.
     * 
     * @param permissionId to set of this RolesPermissions
     * @return             this RolesPermissions instance
     */
    RolesPermissions setPermissionId(Integer permissionId);
    
    /**
     * Queries the specified manager for the referenced Role. If no such Role
     * exists, an {@code NullPointerException} will be thrown.
     * 
     * @param foreignManager the manager to query for the entity
     * @return               the foreign entity referenced
     */
    Optional<Role> findRoleId(Manager<Role> foreignManager);
    
    enum Identifier implements ColumnIdentifier<RolesPermissions> {
        
        ID            ("id"),
        ROLE_ID       ("roleId"),
        PERMISSION_ID ("permissionId");
        
        private final String columnName;
        private final TableIdentifier<RolesPermissions> tableIdentifier;
        
        Identifier(String columnName) {
            this.columnName      = columnName;
            this.tableIdentifier = TableIdentifier.of(    getDbmsName(), 
                getSchemaName(), 
                getTableName());
        }
        
        @Override
        public String getDbmsName() {
            return "springbootspeedment";
        }
        
        @Override
        public String getSchemaName() {
            return "springbootspeedment";
        }
        
        @Override
        public String getTableName() {
            return "RolesPermissions";
        }
        
        @Override
        public String getColumnName() {
            return this.columnName;
        }
        
        @Override
        public TableIdentifier<RolesPermissions> asTableIdentifier() {
            return this.tableIdentifier;
        }
    }
}