package be.manhart.speedment.springbootspeedment.entity.role.generated;

import be.manhart.speedment.springbootspeedment.entity.role.Role;
import be.manhart.speedment.springbootspeedment.entity.role.RoleManager;
import com.speedment.common.annotation.GeneratedCode;
import com.speedment.runtime.config.identifier.TableIdentifier;
import com.speedment.runtime.core.manager.AbstractManager;
import com.speedment.runtime.field.Field;
import java.util.stream.Stream;

/**
 * The generated base implementation for the manager of every {@link
 * be.manhart.speedment.springbootspeedment.entity.role.Role} entity.
 * <p>
 * This file has been automatically generated by Speedment. Any changes made to
 * it will be overwritten.
 * 
 * @author Speedment
 */
@GeneratedCode("Speedment")
public abstract class GeneratedRoleManagerImpl 
extends AbstractManager<Role> 
implements GeneratedRoleManager {
    
    private final TableIdentifier<Role> tableIdentifier;
    
    protected GeneratedRoleManagerImpl() {
        this.tableIdentifier = TableIdentifier.of("springbootspeedment", "springbootspeedment", "Role");
    }
    
    @Override
    public TableIdentifier<Role> getTableIdentifier() {
        return tableIdentifier;
    }
    
    @Override
    public Stream<Field<Role>> fields() {
        return RoleManager.FIELDS.stream();
    }
    
    @Override
    public Stream<Field<Role>> primaryKeyFields() {
        return Stream.of(
            Role.ID
        );
    }
}