package be.manhart.speedment.springbootspeedment.entity.permission;

import be.manhart.speedment.springbootspeedment.entity.permission.generated.GeneratedPermission;

/**
 * The main interface for entities of the {@code Permission}-table in the
 * database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public interface Permission extends GeneratedPermission {
	// for future use - not needed in example
}