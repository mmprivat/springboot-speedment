package be.manhart.speedment.springbootspeedment.entity.usersroles;

import be.manhart.speedment.springbootspeedment.entity.role.Role;
import be.manhart.speedment.springbootspeedment.entity.user.User;
import be.manhart.speedment.springbootspeedment.entity.usersroles.generated.GeneratedUsersRolesManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * be.manhart.speedment.springbootspeedment.entity.usersroles.UsersRoles}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public final class UsersRolesManagerImpl 
extends GeneratedUsersRolesManagerImpl 
implements UsersRolesManager {

	@Override
	public UsersRoles create(User user, Role role) {
		System.out.println("User: " + user + " id: " + user.getId() + " is mapped to role " + role + " id: " + role.getId());
		 return persist(new UsersRolesImpl().setRoleId(role.getId()).setUserId(user.getId()));
	}
}