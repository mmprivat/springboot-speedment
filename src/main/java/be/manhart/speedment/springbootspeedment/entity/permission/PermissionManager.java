package be.manhart.speedment.springbootspeedment.entity.permission;

import be.manhart.speedment.springbootspeedment.entity.permission.generated.GeneratedPermissionManager;

/**
 * The main interface for the manager of every {@link
 * be.manhart.speedment.springbootspeedment.entity.permission.Permission}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public interface PermissionManager extends GeneratedPermissionManager {}