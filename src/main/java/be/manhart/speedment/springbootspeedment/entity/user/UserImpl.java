package be.manhart.speedment.springbootspeedment.entity.user;

import be.manhart.speedment.springbootspeedment.entity.user.generated.GeneratedUserImpl;

/**
 * The default implementation of the {@link
 * be.manhart.speedment.springbootspeedment.entity.user.User}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * <p>
 * 
 * @author be.manhart
 */
public final class UserImpl 
extends GeneratedUserImpl 
implements User {
	public boolean isActive() {
		return isVerified() && isEnabled();
	}

	public boolean isVerified() {
		return TRUE == getVerified().orElse(FALSE);
	}

	public boolean isEnabled() {
		return TRUE == getEnabled().orElse(FALSE);
	}
}