package be.manhart.speedment.springbootspeedment.entity.usersroles;

import be.manhart.speedment.springbootspeedment.entity.usersroles.generated.GeneratedUsersRolesImpl;

/**
 * The default implementation of the {@link
 * be.manhart.speedment.springbootspeedment.entity.usersroles.UsersRoles}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public final class UsersRolesImpl 
extends GeneratedUsersRolesImpl 
implements UsersRoles {}