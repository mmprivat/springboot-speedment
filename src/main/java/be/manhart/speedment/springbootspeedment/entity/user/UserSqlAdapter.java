package be.manhart.speedment.springbootspeedment.entity.user;

import be.manhart.speedment.springbootspeedment.entity.user.generated.GeneratedUserSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * be.manhart.speedment.springbootspeedment.entity.user.User} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public class UserSqlAdapter extends GeneratedUserSqlAdapter {}