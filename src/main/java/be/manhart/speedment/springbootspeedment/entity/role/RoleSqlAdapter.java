package be.manhart.speedment.springbootspeedment.entity.role;

import be.manhart.speedment.springbootspeedment.entity.role.generated.GeneratedRoleSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * be.manhart.speedment.springbootspeedment.entity.role.Role} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public class RoleSqlAdapter extends GeneratedRoleSqlAdapter {}