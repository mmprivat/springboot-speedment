package be.manhart.speedment.springbootspeedment.entity.rolespermissions;

import be.manhart.speedment.springbootspeedment.entity.rolespermissions.generated.GeneratedRolesPermissions;

/**
 * The main interface for entities of the {@code RolesPermissions}-table in the
 * database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public interface RolesPermissions extends GeneratedRolesPermissions {
	// for future use - not needed in example
}