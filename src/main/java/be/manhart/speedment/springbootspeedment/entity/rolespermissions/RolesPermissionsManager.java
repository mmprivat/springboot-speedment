package be.manhart.speedment.springbootspeedment.entity.rolespermissions;

import be.manhart.speedment.springbootspeedment.entity.rolespermissions.generated.GeneratedRolesPermissionsManager;

/**
 * The main interface for the manager of every {@link
 * be.manhart.speedment.springbootspeedment.entity.rolespermissions.RolesPermissions}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public interface RolesPermissionsManager extends GeneratedRolesPermissionsManager {}