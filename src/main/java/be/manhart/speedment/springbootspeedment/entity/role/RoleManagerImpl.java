package be.manhart.speedment.springbootspeedment.entity.role;

import java.util.Optional;

import be.manhart.speedment.springbootspeedment.entity.role.generated.GeneratedRoleManagerImpl;
import be.manhart.speedment.springbootspeedment.exception.EntityNotFoundException;

/**
 * The default implementation for the manager of every {@link
 * be.manhart.speedment.springbootspeedment.entity.role.Role} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public final class RoleManagerImpl 
extends GeneratedRoleManagerImpl 
implements RoleManager {

	@Override
	public Optional<Role> find(String roleName) {
		return stream().filter(Role.NAME.equal(roleName))
				.findAny();
	}
	
	@Override
	public Role get(String roleName) {
		return find(roleName).orElseThrow(() -> new EntityNotFoundException("Role " + roleName + " could not be found."));
	}

	@Override
	public Role create(String roleName) {
		return persist(new RoleImpl().setName(roleName));
	}

	@Override
	public Role findOrCreate(String roleName) {
		return find(roleName).orElseGet(() -> {
			return create(roleName);
		});
	}
}
