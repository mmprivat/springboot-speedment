package be.manhart.speedment.springbootspeedment.entity;

import static org.springframework.security.core.authority.AuthorityUtils.createAuthorityList;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import be.manhart.speedment.springbootspeedment.entity.role.Role;
import be.manhart.speedment.springbootspeedment.entity.user.User;

/**
 * This class is for spring security. It gets a user with his roles.
 * 
 * @author manuel
 */
public class UserWithRoles implements UserDetails {
	private final User user;
	private final List<Role> roles;

	public UserWithRoles(User user, List<Role> roles) {
		this.user = user;
		this.roles = roles;
	}

	/**
	 * Iterates through all {@link Role}s of the current {@link User} and
	 * converts them into spring security roles.
	 * @return {@link Collection} of spring security roles 
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// @formatter:off
		return createAuthorityList(roles.stream()
				.map(Role::getName)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.toArray(String[]::new));
		// @formatter:on
	}

	@Override
	public String getUsername() {
		return user.getEmail().get();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return user.isActive();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return user.isVerified();
	}

	@Override
	public boolean isEnabled() {
		return user.isEnabled();
	}

	@Override
	public String getPassword() {
		return user.getPassword().get();
	}

	public User getUser() {
		return user;
	}

	public List<Role> getRoles() {
		return roles;
	}

	/**
	 * Iterates through all {@link Role}s of the current {@link User} and checks
	 * for the role.
	 * 
	 * @param roleName
	 *            the role he would need
	 * @return true if the given roleName is in the roles of the logged in user
	 */
	public boolean hasRole(String roleName) {
		return roles.stream().anyMatch(r -> r.getName().get().equals(roleName));
	}
}
