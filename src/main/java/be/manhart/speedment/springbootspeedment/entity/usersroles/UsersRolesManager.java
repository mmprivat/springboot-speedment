package be.manhart.speedment.springbootspeedment.entity.usersroles;

import be.manhart.speedment.springbootspeedment.entity.role.Role;
import be.manhart.speedment.springbootspeedment.entity.user.User;
import be.manhart.speedment.springbootspeedment.entity.usersroles.generated.GeneratedUsersRolesManager;

/**
 * The main interface for the manager of every {@link
 * be.manhart.speedment.springbootspeedment.entity.usersroles.UsersRoles}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public interface UsersRolesManager extends GeneratedUsersRolesManager {
	UsersRoles create(User user, Role userRole);
}