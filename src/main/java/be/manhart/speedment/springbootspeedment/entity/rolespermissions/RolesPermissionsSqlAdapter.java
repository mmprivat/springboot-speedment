package be.manhart.speedment.springbootspeedment.entity.rolespermissions;

import be.manhart.speedment.springbootspeedment.entity.rolespermissions.generated.GeneratedRolesPermissionsSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * be.manhart.speedment.springbootspeedment.entity.rolespermissions.RolesPermissions}
 * entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public class RolesPermissionsSqlAdapter extends GeneratedRolesPermissionsSqlAdapter {}