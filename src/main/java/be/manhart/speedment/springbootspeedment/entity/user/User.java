package be.manhart.speedment.springbootspeedment.entity.user;

import be.manhart.speedment.springbootspeedment.entity.user.generated.GeneratedUser;
import be.manhart.speedment.springbootspeedment.entity.usersroles.UsersRoles;

/**
 * The main interface for entities of the {@code User}-table in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * <p>
 * This represents one user. In our example you can create new users (one per
 * unique email).<br>
 * They will disabled and unverified though. So If you like to test the
 * {@link UsersRoles}, you have to set these two to TRUE in the DB first:
 * 
 * @author be.manhart
 */
public interface User extends GeneratedUser {
	public final int TRUE = 1;
	public final int FALSE = 0;

	public boolean isActive();

	public boolean isVerified();

	public boolean isEnabled();
}