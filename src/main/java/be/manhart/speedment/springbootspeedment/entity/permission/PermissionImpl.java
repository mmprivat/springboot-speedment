package be.manhart.speedment.springbootspeedment.entity.permission;

import be.manhart.speedment.springbootspeedment.entity.permission.generated.GeneratedPermissionImpl;

/**
 * The default implementation of the {@link
 * be.manhart.speedment.springbootspeedment.entity.permission.Permission}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public final class PermissionImpl 
extends GeneratedPermissionImpl 
implements Permission {}