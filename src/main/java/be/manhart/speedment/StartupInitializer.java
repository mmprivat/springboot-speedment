package be.manhart.speedment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import be.manhart.speedment.springbootspeedment.entity.role.Role;
import be.manhart.speedment.springbootspeedment.entity.role.RoleManager;

/**
 * This class creates the neccessary roles on startup.
 * 
 * @author manuel
 */
@Component
public class StartupInitializer implements ApplicationListener<ApplicationReadyEvent> {

	private @Autowired RoleManager roles;

	/**
	 * This event is executed as late as conceivably possible to indicate that
	 * the application is ready to service requests.
	 */
	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		roles.findOrCreate(Role.ADMIN);
		roles.findOrCreate(Role.USER);
	}
}
