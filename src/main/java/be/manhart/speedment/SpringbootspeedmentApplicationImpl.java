package be.manhart.speedment;

import be.manhart.speedment.generated.GeneratedSpringbootspeedmentApplicationImpl;

/**
 * The default {@link com.speedment.runtime.core.Speedment} implementation class
 * for the {@link com.speedment.runtime.config.Project} named
 * springbootspeedment.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public final class SpringbootspeedmentApplicationImpl 
extends GeneratedSpringbootspeedmentApplicationImpl 
implements SpringbootspeedmentApplication {}