package be.manhart.speedment;

import be.manhart.speedment.generated.GeneratedSpringbootspeedmentApplication;

/**
 * An {@link com.speedment.runtime.core.ApplicationBuilder} interface for the
 * {@link com.speedment.runtime.config.Project} named springbootspeedment.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author be.manhart
 */
public interface SpringbootspeedmentApplication extends GeneratedSpringbootspeedmentApplication {}