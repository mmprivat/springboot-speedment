package be.manhart;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import be.manhart.speedment.springbootspeedment.entity.UserWithRoles;
import be.manhart.speedment.springbootspeedment.entity.role.Role;
import be.manhart.speedment.springbootspeedment.entity.role.RoleManager;
import be.manhart.speedment.springbootspeedment.entity.user.User;
import be.manhart.speedment.springbootspeedment.entity.user.UserManager;
import be.manhart.speedment.springbootspeedment.entity.usersroles.UsersRoles;
import be.manhart.speedment.springbootspeedment.entity.usersroles.UsersRolesManager;

/**
 * We load the user and his roles and provide them as spring security
 * {@link UserDetails}.
 * 
 * @author manuel
 */
@Configuration
public class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {

	@Autowired
	private UserManager users;

	@Autowired
	private UsersRolesManager usersRoles;

	@Autowired
	private RoleManager roles;

	@Bean
	public DaoAuthenticationProvider authProvider() {
		final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(getUserDetailsService());
		authProvider.setPasswordEncoder(getPasswordEncoder());
		return authProvider;
	}

	// TODO MM see if we can get a better (single line) stream transformation
	// from that
	@Bean
	public UserDetailsService getUserDetailsService() {
		return email -> {
			// TODO MM this should probably go into a manager since it is
			// speedment code
			User user = users.stream().filter(User.EMAIL.equal(email)).findAny().get();

			// @formatter:off
			List<Role> r = usersRoles
				.stream()
				.filter(UsersRoles.USER_ID.equal(user.getId()))
				.collect(Collectors.mapping(
						roles.finderBy(UsersRoles.ROLE_ID),
						Collectors.toList()
					)
			);
			// @formatter:on

			return new UserWithRoles(user, r);
		};
	}

	@Bean
	public PasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(getUserDetailsService()).passwordEncoder(getPasswordEncoder());
	}
}
