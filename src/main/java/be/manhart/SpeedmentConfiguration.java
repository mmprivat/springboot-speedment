package be.manhart;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import be.manhart.speedment.SpringbootspeedmentApplication;
import be.manhart.speedment.SpringbootspeedmentApplicationBuilder;
import be.manhart.speedment.springbootspeedment.entity.permission.PermissionManager;
import be.manhart.speedment.springbootspeedment.entity.role.RoleManager;
import be.manhart.speedment.springbootspeedment.entity.rolespermissions.RolesPermissionsManager;
import be.manhart.speedment.springbootspeedment.entity.user.UserManager;
import be.manhart.speedment.springbootspeedment.entity.usersroles.UsersRolesManager;

/**
 * The speedment config is made here. The variables from {@link Value}
 * annotation are inserted from the application.properties file.
 * 
 * @author manuel
 */
@Configuration
public class SpeedmentConfiguration {

	private @Value("${dbms.host}") String host;
	private @Value("${dbms.port}") int port;
	private @Value("${dbms.schema}") String schema;
	private @Value("${dbms.username}") String username;
	private @Value("${dbms.password}") String password;

	@Bean
	public SpringbootspeedmentApplication getSpeedmentApplication() {
		//@formatter:off
        return new SpringbootspeedmentApplicationBuilder()
            .withIpAddress(host)
            .withPort(port)
            .withUsername(username)
            .withPassword(password)
            .withSchema(schema)
            .build();
        //@formatter:on
	}

	@Bean
	public UserManager getUserManager(SpringbootspeedmentApplication app) {
		return app.getOrThrow(UserManager.class);
	}

	@Bean
	public RoleManager getRoleManager(SpringbootspeedmentApplication app) {
		return app.getOrThrow(RoleManager.class);
	}

	@Bean
	public UsersRolesManager getUsersRolesManager(SpringbootspeedmentApplication app) {
		return app.getOrThrow(UsersRolesManager.class);
	}

	@Bean
	public PermissionManager getPermissionManager(SpringbootspeedmentApplication app) {
		return app.getOrThrow(PermissionManager.class);
	}

	@Bean
	public RolesPermissionsManager getRolesPermissionsManager(SpringbootspeedmentApplication app) {
		return app.getOrThrow(RolesPermissionsManager.class);
	}
}
