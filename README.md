# Springboot with Speedment Example Project

This project will show you how to setup secure Springboot REST services and connect them to the DB via Speedment ORM.

## Getting Started

### Prerequisites

* Java 8+ SDK
* Maven 2+
* some IDE

### Installing

* clone this git repository.
* create a DB named `springbootspeedment`
* execute the `mariadb.sql` script from `src/main/resources/db` on the created DB
* run `mvn clean install`
* run `be.manhart.Application` as java application in your IDE

Now the application is up and running, and you can

## Test the features

### Create a new user

* execute `curl -X POST "http://localhost:9777/user?firstName=John&lastName=Smith&email=john@smith.com&password=testpw"`
* this will create a new user with the credentials `john@smith.com` and `testpw`

### Fetch own user details

* execute `curl -X  GET -u my_john@smith.com:testpw "http://localhost:9777/user/1"`
* this will return that your user is not enabled
* connect to the DB
* execute `update User set enabled = 1, verified = 1 where id = 1;`
* now it will return your user details

### Fetch all user details

* execute `curl -X  GET -u my_john@smith.com:testpw "http://localhost:9777/user"`
* this will not work since your user does not have a mapping to the Admin role (see UsersRoles entity)


## HowTo (Re)Build

### Tools used

* [Springboot](???) - DI, Secure REST Services,...
* [Speedment](???) - Object Relational Mapper
* [Maven](https://maven.apache.org/) - Dependency Management
* [ER-Diagram] (https://www.genmymodel.com/) - Creating the sql for creating the tables

### Steps I took

I followed pretty much the guide from [1] with the neccessary adaptions for my own data model.

## Contributing

No contributing, take it as is and fork your own code from it.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](???). 

## Authors

* **Manuel Manhart** - *Initial work* - [???](https://github.com/PurpleBooth)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

# See also

[1] http://ageofjava.com/java/Secure-REST-API-with-Spring

[2] https://www.speedment.com/developers/